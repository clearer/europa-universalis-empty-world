#!/usr/bin/python3

from pathlib import Path
from pprint import pprint
import sys


if len(sys.argv) != 2:
	print('usage: %s <EU IV root path> <mod root path>' % sys.argv[0])
	sys.exit(1)


tags = [
	'culture',
	'religion',
	'trade_goods',
	'base_tax',
	'base_production',
	'base_manpower',
	'native_size',
	'native_hostileness'
	'capital'
]


sourcepath = Path(sys.argv[1] + '/history/provinces/').resolve()
targetpath = Path(sys.argv[2] + '/history/provinces/').resolve()
pprint([sourcepath, targetpath])

if targetpath.is_file():
	print(targetpath.name + ' exists and is a file')

if not targetpath.exists():
	print(targetpath.name + ' does not exist: creating a new directory')
	targetpath.mkdir(parents = True)

for file in list(sourcepath.glob('*.txt')):
	values = {}
	with file.open(encoding = 'latin-1') as sourcefile:
		lines = [l for l in sourcefile]
		for t in tags:
			try:
				values[t] = [v for v in lines if t in v][0]
			except:
				pass
		
		(targetpath / file.name).write_text(''.join([value for _,value in values.items()]), encoding = 'latin-1')
